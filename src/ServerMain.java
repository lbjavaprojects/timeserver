import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

public class ServerMain {

	public static void main(String[] args) throws IOException {
		try(ServerSocket ss=new ServerSocket(8189);
				Socket s=ss.accept();
				OutputStream os=s.getOutputStream();
				PrintWriter pw=new PrintWriter(os,false)){
			pw.println("Witam na serwerze czasu. Aktualny czas to: "+(new Date()).toString());
			pw.flush();
		}
	}

}
